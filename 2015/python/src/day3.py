#!/usr/bin/python3

import sys

def follow(coordinate, instruction):
    """
    Follows direction of arrow on infinite coordinate grid
    Inputs:
        * coordinate  - current position on grid
        * instruction - direction to move
    Outputs:
        * coordinate  - new position on grid
    """
    if instruction == '>':
        return (coordinate[0] + 1, coordinate[1])
    if instruction == '<':
        return (coordinate[0] - 1, coordinate[1])
    if instruction == '^':
        return (coordinate[0], coordinate[1] + 1)
    if instruction == 'v':
        return (coordinate[0], coordinate[1] - 1)
    return coordinate

def count_unique(instructions):
    """
    Counts the number of unique squares landed on by pointer
    Inputs:
        * instructions - string of instructions to follow
    Outputs:
        * unique - number of unique squares landed on
    """
    coordinate = (0,0)
    landed = [coordinate]
    unique = 1
    for instruction in instructions:
        coordinate = follow(coordinate, instruction)
        if coordinate not in landed:
            unique += 1
            landed.append(coordinate)
    return unique

def count_with_two(instructions):
    """
    Counts unique places with two pointers, which take turns to interpret
    instructions
    Inputs:
        * instructions - string of instructions to follow
    Outputs:
        * unique - number of unique squares landed on
    """
    pointers = [(0,0), (0,0)]
    landed = [pointers[0]]
    unique = 1
    for i in range(len(instructions)):
        which = i % 2
        pointers[which] = follow(pointers[which], instructions[i])
        if pointers[which] not in landed:
            unique += 1
            landed.append(pointers[which])
    return unique


#########
# TESTS #
#########
def test_count():
    tests = ['>', '^>v<', '^v^v^v^v^v']
    print([count_unique(test) for test in tests])

def test_with_two():
    tests = ['^v', '^>v<', '^v^v^v^v^v']
    print([count_with_two(test) for test in tests])

########
# MAIN #
########

if __name__ == '__main__':
    args = sys.argv

    filename = args[1]

    with open(filename, 'r') as f:
        instructions = f.read()

    test_count()
    test_with_two()
    print(count_unique(instructions))
    print(count_with_two(instructions))
