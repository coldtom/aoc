#!/usr/bin/python3

import sys

##########
# PART 1 #
##########

def check_vowels(word):
    """
    Checks a word has at least 3 vowels

    Inputs:
        * word - string to check
    Outputs:
        * check - boolean for if criterion is met
                - True if met, False if not
    """
    vowels = 'aeiou'
    number = 0
    for w in word:
        if w in vowels:
            number += 1
        if number == 3:
            return True
    return False

def check_consecutive(word):
    """
    Checks if a word has two consecutive letters the same

    Inputs:
        * word - string to be checked

    Outputs:
        * check - boolean for if criterion is met
                - True if met, False if not
    """
    if len(word) <= 1:
        return True
    for i in range(1, len(word)):
        if word[i] == word[i - 1]:
            return True
    return False

def check_for_bad(word):
    """
    Checks if a word contains the "bad" subwords

    "bad" subwords: 'ab', 'cd', 'pq', 'xy'

    Inputs:
        * word - string to be checked

    Outputs:
        * check - boolean for if criterion is met
                - True if met, False if not
    """
    bad_subwords = ['ab', 'cd', 'pq', 'xy']
    for subword in bad_subwords:
        if subword in word:
            return True
    return False

def check_nice(word):
    """
    Checks if a word is "nice" or not. A word is "nice" iff it
    satisfies the following criteria:
        * Contains at least 3 vowels
        * Has at least 2 consecutive letters the same
        * Does not contain any "bad" subwords as in check_for_bad()

    Inputs:
        * word - string to be checked

    Outputs:
        * check - boolean for if criteria are met
                - True if met, False if not
    """
    return check_vowels(word) and check_consecutive(word) and not check_for_bad(word)

def get_words(filename):
    """
    Gets words contained in file `filename`

    Inputs:
        * filename - string of filename to be read

    Outputs:
        * words - list of words
    """
    with open(filename, 'r') as f:
        words = [line.rstrip() for line in f.readlines()]
    return words

def count_nice(words, check_function):
    """
    Counts number of nice words in a list

    Inputs:
        * words          - list of words to check
        * check_function - boolean function to check a single word

    Outputs:
        * nice - number of nice words in words
    """
    return sum([int(check_function(word)) for word in words])

##########
# PART 2 #
##########

def check_pair(word):
    """
    Checks the word has a pair of letters that appear twice in
    the word without overlapping

    Inputs:
        * word - string to be checked

    Outputs:
        * check - boolean for if criterion is met
                - True if met, False if not
    """
    pairs = [word[i-1] + word[i] for i in range(1, len(word))]

    for i in range(len(pairs) - 1):
        for j in range(i + 2, len(pairs)):
            if pairs[i] == pairs[j]:
                return True
    return False

def check_alternate(word):
    """
    Checks if word repeats a letter, with one letter in between
    the two instances, e.g. 'xyx'

    Inputs:
        * word - string to be checked

    Outputs:
        * check - boolean for if criterion is met
                - True if met, False if not
    """
    if len(word) <= 2:
        return False
    for i in range(2, len(word)):
        if word[i] == word[i-2]:
            return True
    return False

def check_nice_revised(word):
    """
    Checks if a word is nice with the revised criteria:
        * Contains two instances of a pair of letters without overlap
        * Contains two instances of a letter separated by another, e.g.xyx

    Inputs:
        * word - string to be checked

    Outputs:
        * check - boolean for if criteria are met
                - True if met, False if not
    """
    return check_pair(word) and check_alternate(word)

#########
# TESTS #
#########

def test_vowels():
    # [True, True, True, False]
    tests = ['aei', 'xazegov', 'aeiouaeiouaeiou', 'abba']
    print([check_vowels(test) for test in tests])

def test_consecutive():
    # [True, True, True, False]
    tests = ['xx', 'abcdde', 'aabbccdd', 'abcdef']
    print([check_consecutive(test) for test in tests])

def test_bad():
    # [False, True, True]
    tests = ['xazegov', 'aabbccdd', 'pq']
    print([check_for_bad(test) for test in tests])

def test_nice():
    # [True, True, False, False, False]
    tests = ['ugknbfddgicrmopn', 'aaa', 'jchzalrnumimnmhp', 'haegwjzuvuyypxyu', 'dvszwmarrgswjxmb']
    print([check_nice(test) for test in tests])

def test_pair():
    # [True, True, False, False]
    tests = ['xyxy', 'aabcdefgaa', 'aaa', 'aaabb']
    print([check_pair(test) for test in tests])

def test_alternate():
    # [True, True, True, False]
    tests = ['xyx', 'abcdefeghi', 'aaa', 'abba']
    print([check_alternate(test) for test in tests])

def test_nice_revised():
    # [True, False, False]
    tests = ['qjhvhtzxzqqjkmpb', 'uurcxstgmygtbstg', 'ieodomkazucvgmuy']
    print([check_nice_revised(test) for test in tests])

########
# MAIN #
########

if __name__ == '__main__':
    args = sys.argv

    test_vowels()
    test_consecutive()
    test_bad()
    test_nice()

    print('\n\n')

    test_pair()
    test_alternate()
    test_nice_revised()

    print('\n\n\n')

    filename = args[1]

    words = get_words(filename)
    print(count_nice(words, check_nice))
    print(count_nice(words, check_nice_revised))
