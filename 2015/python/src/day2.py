#!/usr/bin/python3

import sys

def surface(l, w, h):
    """
    Calculates the surface area of an lxwxh cuboid
    with an additional minimal side of slack
    """
    sides = [l*w, w*h, l*h]
    slack = min(sides)
    return 2*sum(sides) + slack

def min_perim(l, w, h):
    """
    Calculates the smallest perimeter of an lxwxh cuboid,
    that is the perimeter of the face with smallest perimeter
    """
    perims = [2*(l+w), 2*(l+h), 2*(w+h)]
    return min(perims)

def volume(l, w, h):
    """
    Calculates the volume of an lxwxh cuboid
    """
    return l*w*h

def ribbon_length(l, w, h):
    """
    Finds the length of ribbon needed, i.e. min perimeter + volume
    """
    return min_perim(l, w, h) + volume(l, w, h)

def parse(line):
    """
    Parses a single line "lxwxh" into integers l, w, h
    """
    items = line.rstrip().split('x')
    return [int(x) for x in items]

def surface_file(filename):
    """
    Calculates total surface area of all boxes in a file
    """
    with open(filename, 'r') as f:
        lines = f.readlines()
    dims = [parse(line) for line in lines]
    area = sum([surface(*dim) for dim in dims])
    return area

def ribbon_file(filename):
    """
    Calculates the total length of ribbon needed for all boxes in a file
    """
    with open(filename, 'r') as f:
        lines = f.readlines()
    dims = [parse(line) for line in lines]
    length = sum([ribbon_length(*dim) for dim in dims])
    return length

#########
# TESTS #
#########

def test_surface():
    print(surface(2, 3, 4))
    print(surface(1, 1, 10))

def test_parse():
    print(*parse('2x3x4\n'))
    print(*parse('1x1x10\n'))

def test_ribbon():
    print(ribbon_length(2, 3, 4))
    print(ribbon_length(1, 1, 10))

########
# MAIN #
########

if __name__ == '__main__':
    args = sys.argv

    filename = args[1]

    print(surface_file(filename))
    print(ribbon_file(filename))
