#!/usr/bin/python3

import sys

##########
# PART 1 #
##########

def count_on(grid):
    """
    Counts number of lights switched on in 1000x10000 grid

    Inputs:
        * grid - current state of grid

    Outputs:
        * int number of lights turned on at present
    """
    return sum([sum([grid[x][y] for y in range(1000)]) for x in range(1000)])

def turn_on(grid, start, end):
    """
    Turns on lights in rectangle with corners start, end

    Inputs:
        * grid  - current state of grid
        * start - bottom left corner of rectangle
        * end   - top right corner of rectangle

    Outputs:
        * grid - new state of grid
    """
    for x in range(start[0], end[0] + 1):
        for y in range(start[1], end[1] + 1):
            grid[x][y] = True
    return grid

def turn_off(grid, start, end):
    """
    Turns off lights in rectangle with corners start, end

    Inputs:
        * grid  - current state of grid
        * start - bottom left corner of rectangle
        * end   - top right corner of rectangle

    Outputs:
        * grid - new state of grid
    """
    for x in range(start[0], end[0] + 1):
        for y in range(start[1], end[1] + 1):
            grid[x][y] = False
    return grid

def toggle(grid, start, end):
    """
    Toggles lights in rectangle with corners start, end

    Inputs:
        * grid  - current state of grid
        * start - bottom left corner of rectangle
        * end   - top right corner of rectangle

    Outputs:
        * grid - new state of grid
    """
    for x in range(start[0], end[0] + 1):
        for y in range(start[1], end[1] + 1):
            grid[x][y] = not grid[x][y]
    return grid

def apply(grid, line):
    """
    Parses a single line of instruction

    Inputs:
        * grid - current state of grid
        * line - string instruction to be parsed

    Outputs:
        * grid - new state of grid
    """
    line = line.split(' ')
    offset = 0

    # If instruction is turn off/on, the coordinate positions
    # in `line` shifts by 1
    if line[0] == 'turn':
        offset = 1

    instruction = line[offset]
    start = [*map(int, line[1 + offset].split(','))]
    end = [*map(int, line[-1].split(','))]

    if instruction == 'on':
        grid = turn_on(grid, start, end)

    if instruction == 'off':
        grid = turn_off(grid, start, end)

    if instruction == 'toggle':
        grid = toggle(grid, start, end)

    return grid

def apply_file(grid, filename):
    """
    Apply all instructions in a given file to a grid

    Inputs:
        * grid     - current state of grid
        * filename - relative path to file

    Outputs:
        * grid - new state of grid
    """
    with open(filename, 'r') as f:
        lines = [line.rstrip() for line in f.readlines()]

    for line in lines:
        grid = apply(grid, line)

    return grid

##########
# PART 2 #
##########

def turn_up(grid, start, end):
    """
    Turns up lights by 1 in rectangle with corners start, end

    Inputs:
        * grid  - current state of grid
        * start - bottom left corner of rectangle
        * end   - top right corner of rectangle

    Outputs:
        * grid - new state of grid
    """
    for x in range(start[0], end[0] + 1):
        for y in range(start[1], end[1] + 1):
            grid[x][y] += 1
    return grid

def turn_down(grid, start, end):
    """
    Turns lights down by 1 in rectangle with corners start, end

    Inputs:
        * grid  - current state of grid
        * start - bottom left corner of rectangle
        * end   - top right corner of rectangle

    Outputs:
        * grid - new state of grid
    """
    for x in range(start[0], end[0] + 1):
        for y in range(start[1], end[1] + 1):
            if grid[x][y] > 0:
                grid[x][y] -= 1
    return grid

def boost(grid, start, end):
    """
    Turns up lights by 2 in rectangle with corners start, end

    Inputs:
        * grid  - current state of grid
        * start - bottom left corner of rectangle
        * end   - top right corner of rectangle

    Outputs:
        * grid - new state of grid
    """
    for x in range(start[0], end[0] + 1):
        for y in range(start[1], end[1] + 1):
            grid[x][y] += 2
    return grid

def apply_revised(grid, line):
    """
    Parses a single line of instruction

    Inputs:
        * grid - current state of grid
        * line - string instruction to be parsed

    Outputs:
        * grid - new state of grid
    """
    line = line.split(' ')
    offset = 0

    # If instruction is turn off/on, the coordinate positions
    # in `line` shifts by 1
    if line[0] == 'turn':
        offset = 1

    instruction = line[offset]
    start = [*map(int, line[1 + offset].split(','))]
    end = [*map(int, line[-1].split(','))]

    if instruction == 'on':
        grid = turn_up(grid, start, end)

    if instruction == 'off':
        grid = turn_down(grid, start, end)

    if instruction == 'toggle':
        grid = boost(grid, start, end)

    return grid

def apply_revised_file(grid, filename):
    """
    Apply all instructions in a given file to a grid

    Inputs:
        * grid     - current state of grid
        * filename - relative path to file

    Outputs:
        * grid - new state of grid
    """
    with open(filename, 'r') as f:
        lines = [line.rstrip() for line in f.readlines()]

    for line in lines:
        grid = apply_revised(grid, line)

    return grid



if __name__ == '__main__':
    args = sys.argv

    kind = args[1]

    filename = args[2]

    grid = [[0 for i in range(1000)] for j in range(1000)]

    if kind == 'initial':
        apply_file(grid, filename)
    if kind == 'revised':
        apply_revised_file(grid, filename)
    print(count_on(grid))
