#!/usr/bin/python3

import sys
import hashlib  # TODO: use own version of MD5 hash

def MD5(key):
    """
    Returns MD5 hash of key in hex

    Inputs:
        * key - key to be hashed
    Outputs:
        * hash - MD5 hash of key
    """
    return hashlib.md5(key.encode('utf-8')).hexdigest()

def has_zeroes(md5_hash, n):
    if md5_hash[:n] == '0' * n:
        return True
    else:
        return False

def find_leading_zeroes(key, n):
    """
    Finds the smallest integer (no leading zeroes) such that the
    key + integer MD5 hash has n leading zeroes

    Inputs:
        * key - key to find least integer for
        * n   - number of leading zeroes
    Outputs:
        * integer - smallest integer that can be appended for a hash with n leading zeroes
    """
    i = 1
    while not has_zeroes(MD5(key + str(i)), n):
        i += 1
    return i

#########
# TESTS #
#########
def test_leading_zeroes():
    tests = ['abcdef', 'pqrstuv']
    print([find_leading_zeroes(test, 5) for test in tests])

########
# MAIN #
########

if __name__ == '__main__':
    args = sys.argv

    filename = args[1]

    test_leading_zeroes()

    with open(filename, 'r') as f:
        key = f.read().rstrip()
    print(key)

    print(find_leading_zeroes(key, 5))
    print(find_leading_zeroes(key, 6))
