use std::boxed::Box;
use std::error::Error;

#[derive(Debug, Eq, PartialEq)]
/// Struct to represent a present.
/// 
/// These presents are all regular rectangular prisms, that is to say boxes. This struct provides
/// various methods used by elves to figure out how much wrapping supplies are required.
/// 
pub struct Present {
    /// Length of the present
    length: usize,

    /// Width of the present
    width: usize,

    /// Height of the present
    height: usize,
}

impl Present {
    /// Create a new Present from the dimensions of the box.
    /// 
    /// ```
    /// # use day2::Present;
    /// 
    /// let present = Present::new(112,234,323);
    /// ```
    pub fn new(length: usize, width: usize, height: usize) -> Self {
        Self {
            length,
            width,
            height,
        }
    }

    /// Create a new Present from a string containing the box dimensions, e.g. "XxYxZ".
    /// 
    /// ```
    /// # use day2::Present;
    /// 
    /// let present = Present::new_from_string("15x16x12");
    /// assert!(present.is_ok());
    /// ```
    pub fn new_from_string(present_str: &str) -> Result<Self, Box<dyn Error>> {
        let dims: Vec<&str> = present_str.split("x").collect();
        let length: usize = dims[0].parse::<usize>()?;
        let width: usize = dims[1].parse::<usize>()?;
        let height: usize = dims[2].parse::<usize>()?;

        Ok(Self {
            length,
            width,
            height,
        })
    }

    /// Calculate the quantity of wrapping paper needed to wrap this present.
    /// 
    /// Calculated as the surface area of the present with an excess the size of the minimal face
    /// of the present.
    /// 
    /// ```
    /// # use day2::Present;
    /// 
    /// let present = Present::new(1,2,3);
    /// assert_eq!(present.wrapping_paper(), 24);
    /// ```
    pub fn wrapping_paper(&self) -> usize {
        let excess: usize = *vec![
            self.length * self.width,
            self.length * self.height,
            self.width * self.height,
        ]
        .iter()
        .min()
        .unwrap();

        2 * self.length * self.width
            + 2 * self.length * self.height
            + 2 * self.width * self.height
            + excess
    }

    /// Calculate the length of ribbon required to wrap this present.
    /// 
    /// Calculated as the smallest face's perimeter plus the volume of the present for a bow.
    /// 
    /// ```
    /// # use day2::Present;
    /// 
    /// let present = Present::new(1,2,3);
    /// assert_eq!(present.ribbon(), 12);
    /// ```
    pub fn ribbon(&self) -> usize {
        self.smallest_face_perimeter() + self.volume()
    }

    /// Find the perimeter of the smallest face
    fn smallest_face_perimeter(&self) -> usize {
        *vec![
            2 * (self.length + self.width),
            2 * (self.length + self.height),
            2 * (self.width + self.height),
        ]
        .iter()
        .min()
        .unwrap()
    }

    /// Find the volume of the present
    fn volume(&self) -> usize {
        self.length * self.width * self.height
    }
}

/// Loads a file of present definitions into a vector of presents
fn load_file<P: AsRef<std::path::Path>>(filename: P) -> Result<Vec<Present>, Box<dyn Error>> {
    let contents = std::fs::read_to_string(filename)?;
    let mut presents = Vec::new();

    for s in contents.lines() {
        let present = Present::new_from_string(s)?;
        presents.push(present);
    }
    Ok(presents)
}

/// Calculates the total wrapping paper required to wrap a Vec of presents.
fn total_wrapping_paper(presents: &Vec<Present>) -> usize {
    presents.iter().map(|p| p.wrapping_paper()).sum()
}

/// Calculates the total length of ribbon needed to wrap a Vec of Presents.
fn total_ribbon(presents: &Vec<Present>) -> usize {
    presents.iter().map(|p| p.ribbon()).sum()
}

fn main() -> Result<(), Box<dyn Error>> {
    let filename = std::env::args().nth(1).ok_or("Expected Filename")?;
    let presents = load_file(filename)?;

    println!("Total Wrapping Paper: {}", total_wrapping_paper(&presents));
    println!("Total Ribbon: {}", total_ribbon(&presents));

    Ok(())
}

#[cfg(test)]
mod day2_tests {
    use super::*;

    #[test]
    fn test_new_present() {
        Present::new(1, 2, 3);
    }

    #[test]
    fn test_new_from_string() {
        let present = Present::new_from_string("1x2x3");
        assert!(present.is_ok());
        assert_eq!(Present::new(1, 2, 3), present.ok().unwrap());
    }

    #[test]
    fn test_wrapping_paper() {
        let present = Present::new(2, 3, 4);
        assert_eq!(present.wrapping_paper(), 58);

        let present = Present::new(1, 1, 10);
        assert_eq!(present.wrapping_paper(), 43)
    }

    #[test]
    fn test_smallest_perimeter() {
        let present = Present::new(2, 3, 4);
        assert_eq!(present.smallest_face_perimeter(), 10);

        let present = Present::new(1, 1, 10);
        assert_eq!(present.smallest_face_perimeter(), 4);
    }

    #[test]
    fn test_volume() {
        let present = Present::new(2, 3, 4);
        assert_eq!(present.volume(), 24);

        let present = Present::new(1, 1, 10);
        assert_eq!(present.volume(), 10);
    }

    #[test]
    fn test_ribbon() {
        let present = Present::new(2, 3, 4);
        assert_eq!(present.ribbon(), 34);

        let present = Present::new(1, 1, 10);
        assert_eq!(present.ribbon(), 14);
    }
}
