use std::boxed::Box;
use std::error::Error;

struct Triangle {
    a: usize,
    b: usize,
    c: usize,
}

impl Triangle {
    fn new(a: usize, b: usize, c: usize) -> Self {
        Self { a, b, c }
    }

    fn from_string(string: String) -> Result<Self, Box<dyn Error>> {
        let params = string.trim().split_whitespace();
        let mut sides: Vec<usize> = Vec::new();
        for param in params {
            let x: usize = param.parse()?;
            sides.push(x);
        }

        assert!(sides.len() == 3);

        Ok(Triangle::new(sides[0], sides[1], sides[2]))
    }

    fn is_possible(&self) -> bool {
        if self.a + self.b <= self.c {
            return false;
        }

        if self.b + self.c <= self.a {
            return false;
        }

        if self.a + self.c <= self.b {
            return false;
        }

        true
    }
}

fn file_to_triangles<P: AsRef<std::path::Path>>(
    filename: P,
) -> Result<Vec<Triangle>, Box<dyn Error>> {
    let contents = std::fs::read_to_string(filename)?;
    Ok(contents
        .lines()
        .filter_map(|l| Triangle::from_string(l.to_string()).ok())
        .collect())
}

fn file_to_triangles_two<P: AsRef<std::path::Path>>(
    filename: P,
) -> Result<Vec<Triangle>, Box<dyn Error>> {
    let contents = std::fs::read_to_string(filename)?;
    let grid: Vec<Vec<usize>> = contents
        .lines()
        .filter_map(|l| l.split_whitespace().map(|s| s.parse().ok()).collect())
        .collect();
    let mut triangles: Vec<Triangle> = Vec::new();

    for i in 0..(grid.len() / 3) {
        for j in 0..3 {
            triangles.push(Triangle::new(
                grid[3 * i][j],
                grid[3 * i + 1][j],
                grid[3 * i + 2][j],
            ));
        }
    }
    Ok(triangles)
}

fn main() -> Result<(), Box<dyn Error>> {
    let filename = std::env::args().nth(1).ok_or("Expected Filename!")?;
    let triangles = file_to_triangles(filename)?;
    println!(
        "Triangles: {}",
        triangles
            .iter()
            .filter(|t| t.is_possible())
            .collect::<Vec<&Triangle>>()
            .len()
    );

    let filename = std::env::args().nth(1).ok_or("Expected Filename!")?;
    let triangles = file_to_triangles_two(filename)?;
    println!(
        "Triangles: {}",
        triangles
            .iter()
            .filter(|t| t.is_possible())
            .collect::<Vec<&Triangle>>()
            .len()
    );
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_triangle_possible() {
        assert!(!Triangle::new(5, 10, 25).is_possible());

        assert!(Triangle::new(3, 4, 5).is_possible());
    }
}
