use std::boxed::Box;
use std::collections::HashSet;
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct SimpleError {
    details: String,
}

impl SimpleError {
    pub fn new(details: &str) -> Self {
        Self {
            details: details.to_string(),
        }
    }
}

impl fmt::Display for SimpleError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for SimpleError {}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn turn(&self, turn: Turn) -> Self {
        match turn {
            Turn::Left => match self {
                Direction::North => Direction::West,
                Direction::East => Direction::North,
                Direction::South => Direction::East,
                Direction::West => Direction::South,
            },
            Turn::Right => match self {
                Direction::North => Direction::East,
                Direction::East => Direction::South,
                Direction::South => Direction::West,
                Direction::West => Direction::North,
            },
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
/// Represents a turn and movement
pub enum Turn {
    /// Represents turning left
    Left,

    /// Represents turning right
    Right,
}

#[derive(Debug, Copy, Clone, Default, Eq, PartialEq, Hash)]
pub struct Coordinate {
    x: isize,
    y: isize,
}

impl Coordinate {
    pub fn new(x: isize, y: isize) -> Self {
        Self { x, y }
    }

    pub fn manhattan(&self, other: Self) -> usize {
        ((self.x - other.x).abs() + (self.y - other.y).abs()) as usize
    }

    pub fn magnitude(&self) -> usize {
        self.manhattan(Self::new(0, 0))
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct DirectedCoordinate {
    coordinate: Coordinate,
    direction: Direction,
}

impl DirectedCoordinate {
    fn new(x: isize, y: isize, direction: Direction) -> Self {
        Self {
            coordinate: Coordinate::new(x, y),
            direction: direction,
        }
    }

    fn apply_instruction(&mut self, instruction: Instruction) {
        self.direction = self.direction.turn(instruction.turn);
        match self.direction {
            Direction::North => self.coordinate.y += instruction.magnitude as isize,
            Direction::East => self.coordinate.x += instruction.magnitude as isize,
            Direction::South => self.coordinate.y -= instruction.magnitude as isize,
            Direction::West => self.coordinate.x -= instruction.magnitude as isize,
        }
    }

    fn get_path(&self, instruction: Instruction) -> Vec<Coordinate> {
        match self.direction.turn(instruction.turn) {
            Direction::North => (0..instruction.magnitude).map(|n| Coordinate::new(self.coordinate.x, self.coordinate.y + n as isize)).collect(),
            Direction::East => (0..instruction.magnitude).map(|n| Coordinate::new(self.coordinate.x + n as isize, self.coordinate.y)).collect(),
            Direction::South => (0..instruction.magnitude).map(|n| Coordinate::new(self.coordinate.x, self.coordinate.y - n as isize)).collect(),
            Direction::West => (0..instruction.magnitude).map(|n| Coordinate::new(self.coordinate.x - n as isize, self.coordinate.y)).collect(),
        }
    }

    fn magnitude(&self) -> usize {
        self.coordinate.magnitude()
    }
}

#[derive(Debug, Copy, Clone)]
struct Instruction {
    turn: Turn,
    magnitude: usize,
}

impl Instruction {
    fn from_string(s: String) -> Result<Self, Box<dyn Error>> {
        let distance: usize = s.chars().skip(1).collect::<String>().parse()?;
        match s.chars().next() {
            Some('R') => Ok(Self {
                turn: Turn::Right,
                magnitude: distance,
            }),
            Some('L') => Ok(Self {
                turn: Turn::Left,
                magnitude: distance,
            }),
            _ => Err(Box::new(SimpleError::new("Error: Invalid instruction"))),
        }
    }
}

fn apply_entire_list(position: &mut DirectedCoordinate, instructions: &Vec<Instruction>) {
    for instruction in instructions {
        position.apply_instruction(*instruction);
    }
}

fn find_first_revisit(position: &mut DirectedCoordinate, instructions: &Vec<Instruction>) -> Coordinate{
    let mut visited: HashSet<Coordinate> = HashSet::new();
    let mut index: usize = 0;

    loop {
        for coordinate in position.get_path(instructions[index]) {
            if !visited.insert(coordinate) {
                return coordinate;
            }
        }
        position.apply_instruction(instructions[index]);
        index = (index + 1) % instructions.len();
    }
}

/// Loads a file of present definitions into a vector of presents
fn load_file<P: AsRef<std::path::Path>>(filename: P) -> Result<Vec<Instruction>, Box<dyn Error>> {
    let contents = std::fs::read_to_string(filename)?;
    let mut instructions = Vec::new();
    for s in contents.split(',') {
        let instruction = Instruction::from_string(s.trim().to_string())?;
        instructions.push(instruction);
    }

    Ok(instructions)
}

fn main() -> Result<(), Box<dyn Error>> {
    let filename = std::env::args().nth(1).ok_or("Expected Filename.")?;
    let instructions = load_file(filename)?;

    let mut position = DirectedCoordinate::new(0, 0, Direction::East);
    apply_entire_list(&mut position, &instructions);
    println!("Distance: {}", position.magnitude());

    let mut position = DirectedCoordinate::new(0, 0, Direction::East);
    let repeat = find_first_revisit(&mut position, &instructions);
    println!("Correct Distance: {}", repeat.magnitude());

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_apply_instruction() {
        let mut position = DirectedCoordinate::new(0,0,Direction::East);
        let instruction = Instruction::from_string("R3".to_string()).unwrap();

        position.apply_instruction(instruction);

        assert_eq!(DirectedCoordinate::new(0, -3, Direction::South), position);
    }

    #[test]
    fn test_first_revisit() {
        let mut position = DirectedCoordinate::new(0,0,Direction::North);
        let instructions = vec![Instruction {
            turn: Turn::Right,
            magnitude: 8, 
        },
        Instruction {
            turn: Turn::Right,
            magnitude: 4,
        },
        Instruction {
            turn: Turn::Right,
            magnitude: 4,
        },
        Instruction {
            turn: Turn::Right,
            magnitude: 8,
        },
        ];

        let repeat = find_first_revisit(&mut position, &instructions);
        assert_eq!(repeat, Coordinate::new(4, 0));
    }
}
