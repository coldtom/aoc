use std::str;
use crypto::digest::Digest;
use crypto::md5::Md5;

fn get_password_simple(door_id: &[u8], prefix: &str) -> String {
    let mut password = "".to_string();
    let mut index = 0;
    let mut hasher = Md5::new();

    while password.len() < 8 {
        hasher.input(door_id);
        hasher.input(index.to_string().as_bytes());
        let digest = hasher.result_str();
        if digest.starts_with(prefix) {
            password.push(digest.chars().nth(prefix.len()).unwrap());
            println!("{}", password);
        }
        hasher.reset();
        index += 1;
    }

    password
}

fn get_password_complex(door_id: &[u8], prefix: &str) -> String {
    let mut password = ['_'; 8];
    let mut index = 0;
    let mut hasher = Md5::new();

    while password.iter().any(|c| *c == '_') {
        hasher.input(door_id);
        hasher.input(index.to_string().as_bytes());
        let digest = hasher.result_str();
        if digest.starts_with(prefix) {
            let position = digest.chars().nth(prefix.len()).unwrap().to_digit(16).unwrap() as usize;
            if position < 0x8  && password[position] == '_' {
                password[position] = digest.chars().nth(prefix.len() + 1).unwrap();
                println!("{}", password.iter().collect::<String>());
            }
        }
        hasher.reset();
        index += 1;
    }

    password.iter().collect::<String>()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let door_id = std::env::args().nth(1).ok_or("Expected door ID")?;
    let prefix = std::env::args().nth(2).ok_or("Expected prefix")?;

    println!("door_id: {}", door_id);
    println!("password: {}", get_password_simple(door_id.as_bytes(), &prefix));
    println!("second password: {}", get_password_complex(door_id.as_bytes(), &prefix));

    Ok(())
}
