use std::cmp::Ordering;
use std::collections::HashMap;
use std::convert::TryFrom;

use regex::Regex;

fn compare_letter_count(a: &(char, usize), b: &(char, usize)) -> Ordering {
    if a.1.gt(&b.1) {
        return Ordering::Less;
    }
    if a.1.lt(&b.1) {
        return Ordering::Greater;
    }
    if a.0.lt(&b.0) {
        return Ordering::Less;
    }
    if a.0.gt(&b.0) {
        Ordering::Greater
    }
    else {
        Ordering::Equal
    }
}

fn letter_shift(c: char, shift: usize) -> char {
    if c == '-' {
        return ' ';
    }

    (b'a' as usize + (c as usize + shift - b'a' as usize) % 26) as u8 as char
}

fn letter_count(string: &str) -> HashMap<char, usize> {
    let mut letter_counts = HashMap::new();

    for c in string.chars().filter(|c| c.is_alphabetic()) {
        letter_counts.entry(c).and_modify(|n| *n += 1).or_insert(1);
    }

    letter_counts
}

#[derive(Debug)]
struct Room {
    name: String,
    id: usize,
    checksum: String,
}

impl Room {
    fn valid_checksum(&self) -> bool {
        let mut letter_counts = letter_count(&self.name).iter().map(|(&a, &b)| (a, b)).collect::<Vec<(char, usize)>>();

        letter_counts.sort_by(compare_letter_count);

        letter_counts.iter().take(5).map(|(a, _)| a).collect::<String>() == self.checksum
    }

    fn decrypt_name(&self) -> String {
        self.name.chars().map(|c| letter_shift(c, self.id)).collect()
    }
}

impl TryFrom<&str> for Room {
    type Error = &'static str;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let re = Regex::new(r"([a-z\-]+)-([\d]+)\[([a-z]+)\]").unwrap();
        let caps = match re.captures(value) {
            Some(c) => c,
            None => return Err("Room didn't match Regex!")
        };

        let name = caps.get(1).map(|m| m.as_str().to_string()).ok_or("No name found!")?;
        let id = match caps.get(2).map(|m| m.as_str().parse::<usize>()).ok_or("No ID found!")?{
            Ok(n) => n,
            Err(_) => return Err("Failed to Parse ID, expected integer"),
        };
        let checksum = caps.get(3).map(|m| m.as_str().to_string()).ok_or("No checksum found!")?;

        Ok(Self{
            name,
            id,
            checksum,
        })
    }
}


fn main() -> Result<(), Box<dyn std::error::Error>>{
    let filename = std::env::args().nth(1).ok_or("Expected Filename!")?;
    let contents = std::fs::read_to_string(filename)?;

    let rooms = contents.lines().map(Room::try_from).collect::<Result<Vec<Room>, &str>>()?;

    println!("Sum of valid IDs: {}", rooms.iter().filter(|r| r.valid_checksum()).map(|r| r.id).sum::<usize>());

    for room in rooms.iter().filter(|r| r.valid_checksum()) {
        println!("{}: {}", room.decrypt_name(), room.id);
}
    Ok(())
}
