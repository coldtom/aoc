use std::boxed::Box;
use std::error::Error;

#[derive(Debug, Eq, PartialEq, Clone)]
enum Instruction {
    Up,
    Down,
    Left,
    Right,
}

impl Instruction {
    fn from_char(c: char) -> Option<Self> {
        match c {
            'U' => Some(Self::Up),
            'D' => Some(Self::Down),
            'L' => Some(Self::Left),
            'R' => Some(Self::Right),
            _ => None,
        }
    }
}

trait KeyPad {
    fn position(&self) -> usize;

    fn up(&mut self);
    fn down(&mut self);
    fn left(&mut self);
    fn right(&mut self);

    fn follow_command(&mut self, instructions: Vec<Instruction>) -> usize {
        for instruction in instructions {
            match instruction {
                Instruction::Up => self.up(),
                Instruction::Down => self.down(),
                Instruction::Left => self.left(),
                Instruction::Right => self.right(),
            };
        }
        self.position()
    }
}

#[derive(Debug, Eq, PartialEq)]
struct SimpleKeyPad {
    width: usize,
    height: usize,
    position: usize,
}

impl SimpleKeyPad {
    fn new(width: usize, height: usize, position: usize) -> Self {
        Self {
            width,
            height,
            position,
        }
    }
}

impl KeyPad for SimpleKeyPad {
    fn position(&self) -> usize {
        self.position
    }

    fn up(&mut self) {
        if self.position <= self.width {
            return ();
        }
        self.position -= self.width;
    }

    fn down(&mut self) {
        if self.position > self.width * (self.height - 1) {
            return ();
        }
        self.position += self.width;
    }

    fn right(&mut self) {
        if self.position % self.width == 0 {
            return ();
        }
        self.position += 1;
    }

    fn left(&mut self) {
        if self.position % self.width == 1 {
            return ();
        }
        self.position -= 1;
    }
}

const GRID: [char; 25] = [
            '_', '_', '1', '_', '_',
            '_', '2', '3', '4', '_',
            '5', '6', '7', '8', '9',
            '_', 'A', 'B', 'C', '_',
            '_', '_', 'D', '_', '_',
        ];


#[derive(Debug, Eq, PartialEq)]
struct FancyKeyPad {
    position: usize,
}

impl FancyKeyPad {
    fn new() -> Self {
        Self {
            position: 11,
        }
    }

    fn is_empty(&self, position: usize) -> bool {
        if position > 25 || GRID[position - 1] == '_' {
            return true;
        }
        false
    }
}

impl KeyPad for FancyKeyPad {
    fn position(&self) -> usize {
        self.position
    }

    fn up(&mut self) {
        if self.position <= 5 || self.is_empty(self.position - 5) {
            return ();
        }
        self.position -= 5;
    }

    fn down(&mut self) {
        if self.position >= 21 || self.is_empty(self.position + 5) {
            return ();
        }
        self.position += 5;
    }

    fn right(&mut self) {
        if self.position % 5 == 0 || self.is_empty(self.position + 1) {
            return ();
        }
        self.position += 1;
    }

    fn left(&mut self) {
        if self.position % 5 == 1 || self.is_empty(self.position - 1) {
            return ();
        }
        self.position -= 1;
    }
}

fn file_to_commands<P: AsRef<std::path::Path>>(
    filename: P,
) -> Result<Vec<Vec<Instruction>>, Box<dyn Error>> {
    let contents = std::fs::read_to_string(filename)?;
    let commands = contents
        .lines()
        .map(|l| {
            l.chars()
                .filter_map(|c| Instruction::from_char(c))
                .collect()
        })
        .collect();
    Ok(commands)
}

fn main() -> Result<(), Box<dyn Error>> {
    let filename = std::env::args().nth(1).ok_or("Expected Filename!")?;
    let commands = file_to_commands(filename)?;

    let mut keys = SimpleKeyPad::new(3, 3, 5);

    let code: String = commands
        .iter()
        .map(|v| keys.follow_command(v.to_vec()).to_string())
        .collect();

    println!("Bathroom Code: {}", code);

    let mut fancy_keys = FancyKeyPad::new();

    let code: String = commands
        .iter()
        .map(|v| GRID[fancy_keys.follow_command(v.to_vec()) - 1])
        .collect();

    println!("True Bathroom Code: {}", code);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn string_to_command(string: &str) -> Vec<Instruction> {
        string
            .chars()
            .filter_map(|c| Instruction::from_char(c))
            .collect()
    }

    #[test]
    fn test_instruction_parsing() {
        assert_eq!(Instruction::Up, Instruction::from_char('U').unwrap());
        assert_eq!(Instruction::Down, Instruction::from_char('D').unwrap());
        assert_eq!(Instruction::Left, Instruction::from_char('L').unwrap());
        assert_eq!(Instruction::Right, Instruction::from_char('R').unwrap());
    }

    #[test]
    fn test_keypad_movement_simple() {
        let mut keys = SimpleKeyPad::new(3, 3, 5);

        keys.up();
        assert_eq!(keys.position(), 2);

        keys.left();
        assert_eq!(keys.position(), 1);

        keys.down();
        assert_eq!(keys.position(), 4);

        keys.right();
        assert_eq!(keys.position(), 5);
    }

    #[test]
    fn test_keypad_limits_simple() {
        let mut keys = SimpleKeyPad::new(3, 3, 1);

        keys.up();
        assert_eq!(keys.position(), 1);

        keys.left();
        assert_eq!(keys.position(), 1);

        keys.right();
        keys.right();
        keys.right();
        assert_eq!(keys.position(), 3);

        keys.down();
        keys.down();
        keys.down();
        assert_eq!(keys.position(), 9);
    }

    #[test]
    fn test_full_sequence_simple() {
        let mut keys = SimpleKeyPad::new(3, 3, 5);

        keys.follow_command(string_to_command("ULL"));
        assert_eq!(keys.position(), 1);
        keys.follow_command(string_to_command("RRDDD"));
        assert_eq!(keys.position(), 9);
        keys.follow_command(string_to_command("LURDL"));
        assert_eq!(keys.position(), 8);
        keys.follow_command(string_to_command("UUUUD"));
        assert_eq!(keys.position(), 5);
    }

    #[test]
    fn test_keypad_movement_fancy() {
        let mut keys = FancyKeyPad::new();

        keys.right();
        keys.right();
        assert_eq!(keys.position(), 13);

        keys.left();
        assert_eq!(keys.position(), 12);

        keys.up();
        assert_eq!(keys.position(), 7);

        keys.down();
        assert_eq!(keys.position(), 12);
    }

    #[test]
    fn test_full_sequence_fancy() {
        let mut keys = FancyKeyPad::new();

        keys.follow_command(string_to_command("ULL"));
        assert_eq!(GRID[keys.position() - 1], '5');
        keys.follow_command(string_to_command("RRDDD"));
        assert_eq!(GRID[keys.position() - 1], 'D');
        keys.follow_command(string_to_command("LURDL"));
        assert_eq!(GRID[keys.position() - 1], 'B');
        keys.follow_command(string_to_command("UUUUD"));
        assert_eq!(GRID[keys.position() - 1], '3');
    }

}
