//! Advent of Code 2019: Day 1
//!
//! Calculates the total fuel required to launch a rocket based on a list of masses of all the
//! component modules.
//!
//! A module of mass `M` requires a volume of fuel which weighs `floor(M / 3) - 2`, however we need
//! to take into account this fuel's weight too.

/// Convenience wrapper to load a file with an integer on each line into a Vec<usize>
fn load_file_to_usize_vec<P: AsRef<std::path::Path>>(
    filename: P,
) -> Result<Vec<usize>, std::boxed::Box<dyn std::error::Error>> {
    let contents = std::fs::read_to_string(filename)?;
    let mut numbers = Vec::new();

    for s in contents.lines() {
        let number: usize = s.parse()?;
        numbers.push(number);
    }
    Ok(numbers)
}

/// Calculates the fuel required for a single mass
fn fuel_required(mass: usize) -> usize {
    if mass <= 6 {
        return 0;
    }
    mass / 3 - 2
}

/// Calculates the total fuel required for a module, including fuel for the fuel
fn module_fuel(mass: usize) -> usize {
    let mut current_mass = mass;
    let mut fuel: usize = 0;

    while current_mass != 0 {
        current_mass = fuel_required(current_mass);
        fuel += current_mass;
    }

    fuel
}

/// Calculates the total mass of fuel required for a list of masses.
fn total_fuel(masses: Vec<usize>) -> usize {
    masses.iter().map(|m| module_fuel(*m)).sum()
}

fn main() -> Result<(), std::boxed::Box<dyn std::error::Error>> {
    let filename = std::env::args().nth(1).ok_or("Expected Filename")?;
    let contents = load_file_to_usize_vec(filename)?;

    println!("Total Fuel: {}", total_fuel(contents));
    Ok(())
}

#[cfg(test)]
mod day1_tests {
    use super::*;

    #[test]
    fn test_fuel_required() {
        assert_eq!(fuel_required(12), 2);
        assert_eq!(fuel_required(14), 2);
        assert_eq!(fuel_required(1969), 654);
        assert_eq!(fuel_required(100756), 33583);
    }

    #[test]
    fn test_module_fuel() {
        assert_eq!(module_fuel(14), 2);
        assert_eq!(module_fuel(1969), 966);
        assert_eq!(module_fuel(100756), 50346);
    }
}
