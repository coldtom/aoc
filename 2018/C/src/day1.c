#include <stdio.h>
#include <stdlib.h>

#define MAX_FILE_SIZE 1000
#define DATA_FILE "./data/day1.txt"
#define MAX_PREVIOUS_FREQS 1000000000

int change_frequency (const int changes[MAX_FILE_SIZE])
{
  int freq = 0;

  for (int i = 0; i < MAX_FILE_SIZE; i++)
    {
      freq += changes[i];
    }
  return freq;
}

int check_in_list (int check, int length, const int *list)
{
  for (int i = 0; i < length; i++)
    {
      if (list[i] == check)
        {
          return 1;
        }
    }
  return 0;
}

int find_duplicate (const int changes[MAX_FILE_SIZE])
{
  int freq = 0, length = 0, index = 0;
  int *previous = malloc(MAX_PREVIOUS_FREQS * sizeof(int));

  while (check_in_list (freq, length, previous) != 1)
    {
      if (changes[index] != 0)
        {
          previous[length] = freq;
          length++;
        }
      freq += changes[index];
      index = (index + 1) % MAX_FILE_SIZE;
    }

  return freq;
}

int * load_file (const char *filename)
{
  int *contents = malloc(MAX_FILE_SIZE * sizeof(int));
  int i = 0, n = 0;

  FILE *f = fopen (filename, "r");

  while (fscanf (f, "%d\n", &n) > 0)
    {
      contents[i++] = n;
    }

  if (i < MAX_FILE_SIZE)
    {
      for (int j = i; j < MAX_FILE_SIZE; j++)
        {
          contents[i++] = 0;
        }
    }

  fclose (f);

  return contents;
}

void test_change_frequency ()
{
  int test1[MAX_FILE_SIZE] = {0}, test2[MAX_FILE_SIZE] = {0},
      test3[MAX_FILE_SIZE] = {0};

  test1[0] = 1;
  test1[1] = 1;
  test1[2] = 1;

  test2[0] = 1;
  test2[1] = 1;
  test2[2] = -2;

  test3[0] = -1;
  test3[1] = -2;
  test3[2] = -3;

  printf ("%d\n", change_frequency (test1));
  printf ("%d\n", change_frequency (test2));
  printf ("%d\n", change_frequency (test3));
}

void test_find_duplicate ()
{
 int test1[MAX_FILE_SIZE] = {0}, test2[MAX_FILE_SIZE] = {0},
     test3[MAX_FILE_SIZE] = {0}, test4[MAX_FILE_SIZE] = {0};

  test1[0] = 1;
  test1[1] = -1;

  test2[0] = 3;
  test2[1] = 3;
  test2[2] = 4;
  test2[3] = -2;
  test2[4] = -4;

  test3[0] = -6;
  test3[1] = 3;
  test3[2] = 8;
  test3[3] = 5;
  test3[4] = -6;

  test4[0] = 7;
  test4[1] = 7;
  test4[2] = -2;
  test4[3] = -7;
  test4[4] = -4;

  printf ("%d\n", find_duplicate (test1));
  printf ("%d\n", find_duplicate (test2));
  printf ("%d\n", find_duplicate (test3));
  printf ("%d\n", find_duplicate (test4));
}

int main ()
{
  test_change_frequency ();
  test_find_duplicate ();
  int *contents = load_file (DATA_FILE);
  int freq = change_frequency (contents);
  int dup = find_duplicate (contents);

  printf ("\n%d\n", freq);
  printf ("\n%d\n", dup);

  return 0;
}
