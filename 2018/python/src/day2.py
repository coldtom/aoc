#!/usr/bin/python

import sys

def find_n_letters(word, n):
    """
    Checks if a word has exactly n of the same letter

    Args:
        * word (string): word to be checked
        * n (int): number of instances of the same letter

    Returns:
        * check (bool): True if there are n of the same letter
    """
    for letter in word:
        if word.count(letter) == n:
            return True
    return False

def count_words(words, n):
    """
    Counts number of words in a list with n of the same letter.

    Args:
        * words (list of string): list of words to be checked
        * n (int): number of instances of the same letter

    Returns:
        * number (int): number of words with exactly n of the same letter
    """
    return len([word for word in words if find_n_letters(word, n)])

def checksum(words):
    """
    Multiplies number of words with 2 of the same letter by number or words
    with 3 of the same letter

    Args:
        * words (list of string): list of words to be checksummed

    Returns:
        * checksum (int): checksum of the words
    """
    return count_words(words, 2) * count_words(words, 3)

def compare(word1, word2):
    """
    Finds the position-wise difference between two words, and also the last
    position that was different

    Args:
        * word1 (string): first word to be compared
        * word2 (string): second word to be compared

    Returns:
        * count (int): number of differences between word1, word2
        * last (int): position of the last difference
    """
    count = 0
    last = 0
    for i in range(len(word1)):
        if word1[i] != word2[i]:
            count += 1
            last = i
    return count, last

def find_matching(words):
    """
    Finds the 'unique' pair of words with only one position different

    Args:
        * words (list of string): list of words to find pair from

    Returns:
        * word (string): subword which is the commonality between the pair
    """
    for i in range(len(words)):
        for j in range(i, len(words)):
            count, last = compare(words[i], words[j])
            if count == 1:
                # Since we are only interested in the case with one difference
                # we can just use the last index, as it's the only one
                return words[i][:last] + words[i][last+1:]


if __name__ == '__main__':
    args = sys.argv

    filename = args[1]

    with open(filename, 'r') as f:
        words = [line.rstrip() for line in f.readlines()]

    print(checksum(words))
    print(find_matching(words))
