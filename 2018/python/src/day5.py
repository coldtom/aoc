#!/usr/bin/python

import sys
import string

def reduce_once(polymer):
    """
    Runs through the polymer and removes any instances xX or Xx

    Args:
        * polymer (string): polymer to be reduced

    Returns:
        * reduced (string): polymer after a single level of reduction
    """
    to_pop = []
    for i in range(len(polymer)-1):
        if polymer[i] == polymer[i+1].swapcase() and i not in to_pop:
            to_pop.append(i)
            to_pop.append(i+1)

    for i, _ in enumerate(to_pop):
        polymer = polymer[:to_pop[i]-i] + polymer[(to_pop[i] - i)+1:]

    return polymer

def reduce(polymer):
    """
    Fully reduces a polymer.

    Args:
        * polymer (string): polymer to be reduced

    Returns:
        * reduced (string): fully reduced polymer
    """
    reduced = reduce_once(polymer)

    while reduced != polymer:
        polymer = reduced
        reduced = reduce_once(polymer)

    return reduced

def remove_unit(polymer, unit):
    """
    Removes all x/X units from polymer.

    Args:
        * polymer (string): polymer to be processed
        * unit (char): unit type to be removed

    Returns:
        * polymer (string): polymer with units removed
    """
    polymer = polymer.replace(unit, '')
    polymer = polymer.replace(unit.swapcase(), '')
    return polymer

def find_optimal(polymer):
    """
    Finds optimal unit to remove from polymer

    Args:
        * polymer (string): polymer to be optimised

    Returns:
        * unit (char): optimal unit to remove
        * length (int): length of reduced polymer
    """
    alphadict = {}
    for x in string.ascii_lowercase:
        removed = remove_unit(polymer, x)
        print(len(removed))
        alphadict[x] = len(reduce(removed))

    optimal = min(alphadict, key=alphadict.get)

    return optimal, alphadict[optimal]

if __name__ == '__main__':
    args = sys.argv

    filename = args[1]

    with open(filename, 'r') as f:
        polymer = f.readline().rstrip()

    print(len(reduce(polymer)))
    print(find_optimal(polymer))
