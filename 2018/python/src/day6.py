#!/usr/bin/python

import sys

def manhattandistance(x, y):
    """
    Returns the manhattan distance between x and y.

    Args:
        * x (list of int): first coordinate
        * y (list of int): second coordinate

    Returns:
        * distance (int): distance between x and y
    """
    if len(x) != len(y):
        raise ValueError('Coordinates must have same length')
    return sum([abs(x[i] - y[i]) for i in range(len(x))])

def find_closest(x, coords):
    """
    Finds closest coordinate in coords to x.

    Args:
        * x (list of int): coordinate in question
        * coords (list of list): list of coordinates to find closest to x

    Returns:
        * coord (int): index of closest coordinate in coords to x
    """
    distances = [manhattandistance(x, coord) for coord in coords]

    min_dist = min(distances)

    if distances.count(min_dist) > 1:
        return -1
    return distances.index(min_dist)

def find_area(grid, index):
    """
    Finds area of coordinate with index index. If the area touches the outside
    edge, returns -1.

    Args:
        * grid (list of list): coordinate grid, with each value being index of
                               closest index
        * index (int): index to find area of

    Returns:
        * area (int): area associated with coordinate in index index
    """
    if index in grid[0] or index in grid[-1]:
        return -1
    if index in [grid[x][0] for x in range(len(grid))] \
                 or index in [grid[x][-1] for x in range(len(grid))]:
        return -1
    return (sum([row.count(index) for row in grid]))

def generate_grid(coords):
    max_x = max([coord[0] for coord in coords]) + 1
    max_y = max([coord[1] for coord in coords]) + 1

    grid = [[] for x in range(max_x)]

    for x in range(max_x):
        for y in range(max_y):
            grid[x].append(find_closest([x, y], coords))
    return grid

def read_file(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
    coords = [[int(line.split(',')[0]), int(line.split(',')[1])]
                                            for line in lines]
    return coords

def get_max_area(grid, coords):
    return max([find_area(grid, index) for index in range(len(coords))])

def get_total_distance(x, coords):
    return sum([manhattandistance(x, coord) for coord in coords])

def get_safe(grid):
    distances = []
    for row in grid:
        for column in row:
            distances.append(column)
    return distances.count(True)

def generate_safe_grid(coords):
    max_x = max([coord[0] for coord in coords]) + 1
    max_y = max([coord[1] for coord in coords]) + 1

    grid = [[] for x in range(max_x)]

    for x in range(max_x):
        for y in range(max_y):
            grid[x].append(get_total_distance([x, y], coords) < 10000)
    return grid

if __name__ == '__main__':
    args = sys.argv

    filename = args[1]

    coords = read_file(filename)
    grid = generate_grid(coords)
    print(get_max_area(grid, coords))

    safe_grid = generate_safe_grid(coords)
    print(get_safe(safe_grid))
