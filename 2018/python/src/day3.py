#!/usr/bin/python3

import sys

def create_rect(coords, size):
    """
    Given a rectangle of size `size` positioned at coordinate coords, returns
    a list of all the coordinates covered by the rectangle

    Args:
        * coords (list of int): coordinates of rectangle
        * size (list of int): dimensions of rectangle

    Returns:
        * coordinates (list of list of int): coordinates covered by rectangle
    """
    coordinates = []
    for i in range(size[0]):
        for j in range(size[1]):
            coordinates.append([coords[0] + i, coords[1] + j])
    return coordinates

def count_overlaps(rectangles):
    """
    Counts the number of overlaps in a list of rectangles


    Args:
        * rectangles (list of list): list of rectangles given from `create_rect`

    Returns:
        * overlaps (int): number of coordinates which are in two or more rects
    """
    coordinates = []
    for rectangle in rectangles:
        coordinates += rectangle
    print('created coordinates')

    overlaps = [[], []]
    n_overlaps = 0
    for coordinate in coordinates:
        if coordinate in overlaps[0]:
            if coordinate not in overlaps[1]:
                overlaps[1].append(coordinate)
                n_overlaps += 1
        else:
            overlaps[0].append(coordinate)

    for rectangle in rectangles:
        flag = False
        for coord in rectangle:
            if coord in overlaps[1]:
                flag = True
        if flag == False:
            id = rectangles.index(rectangle) + 1
    #counts = {str(x):coordinates.count(x) for x in coordinates}
    #print('created counts')

    #overlaps = len([counts[coord] for coord in counts.keys() if counts[coord] > 1])

    return n_overlaps, id

def parse_claim(claim):
    """
    Parse a single string in the form of a claim into concrete data

    Args:
        * claim (string): A string in the form of a claim

    Returns:
        * id (int): ID of the claim
        * coords (list of int): Coordinates of the rectangle
        * area (list of int): Area of the rectangle
    """
    claim = claim[1:].split('@')    # Strip the '#', split on '@'
    id = int(claim[0].rstrip())

    claim = claim[1].split(':')     # Split second half on ':'
    coords = [int(x) for x in claim[0].split(',')]
    area = [int(x) for x in claim[1].split('x')]

    return id, coords, area

def parse_file(filename):
    """Parse a file of claims into a usable format

    Args:
        * filename (string): path to file to be parsed

    Returns:
        * claims (list): list containing id, coords, area of each claim
    """
    with open(filename, 'r') as f:
       claims = [parse_claim(line.rstrip()) for line in f.readlines()]
    return claims

def overlap_file(filename):
    data = parse_file(filename)
    print('loaded file')
    rectangles = [create_rect(claim[1], claim[2]) for claim in data]
    print('created rectangles')
    return count_overlaps(rectangles)

def test_overlaps():
    test_data = ['#1 @ 1,3: 4x4','#2 @ 3,1: 4x4','#3 @ 5,5: 2x2']
    usable_data = [parse_claim(test) for test in test_data]
    rectangles = [create_rect(claim[1], claim[2]) for claim in usable_data]
    print(count_overlaps(rectangles))


if __name__ == '__main__':
    test_overlaps()

    args = sys.argv

    filename = args[1]
    print(overlap_file(filename))
