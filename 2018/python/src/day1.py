#!/usr/bin/python3

import sys

def change_frequency(changes, freq=0):
    """
    Applies a list of frequency changes to an initial frequency

    Args:
        * changes (list of int): Changes to be applied
        * freq (int): Initial frequency

    Returns:
        * freq (int): frequency after changes are applied
    """
    return freq + sum(changes)

def find_duplicate(changes, freq=0):
    """
    Applies a list of frequency changes until a duplicate frequency
    is found.

    Args:
        * changes (list of int): Changes to be applied
        * freq (int): Initial frequency

    Returns:
        * freq (int): First frequency to be reached twice
    """
    frequencies = set()
    index = 0
    while freq not in frequencies:
        frequencies.add(freq)
        freq += changes[index]
        index = (index + 1) % len(changes)
    return(freq)


def load_file(filename):
    """
    Loads a file holding a list of ints

    Args:
        * filename (string): Path to file to be loaded

    Returns:
        * contents (list of int): Contents of the loaded file
    """
    with open(filename, 'r') as f:
        contents = [int(i.rstrip()) for i in f.readlines()]
    return contents

def test_change_frequency():
    tests = [[1,1,1], [1,1,-2], [-1,-2,-3]]
    print([change_frequency(test) for test in tests])

def test_find_duplicate():
    tests = [[1,-1], [3,3,4,-2,-4], [-6,3,8,5,-6], [7,7,-2,-7,-4]]
    print([find_duplicate(test) for test in tests])

if __name__ == '__main__':
    args = sys.argv

    if len(args) > 1:
        kind = args[1]
        filename = args[2]
        changes = load_file(filename)
        if kind == 'add':
            freq = change_frequency(changes)
            print(freq)
        if kind == 'duplicate':
            freq = find_duplicate(changes)
            print(freq)

    else:
        test_change_frequency()
        test_find_duplicate()
