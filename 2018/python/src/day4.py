#!/usr/bin/python

from datetime import datetime, timedelta
from statistics import mode
import sys

def parse_record(line, previous):
    line = line.strip().split(']')
    time = datetime.strptime(line[0], '[%Y-%m-%d %H:%M')

    line = line[1].strip().split(' ')
    if line[0] == 'Guard':
        guard = int(line[1][1:])
    else:
        guard = previous[1]

    if line[-1] == 'asleep':
        asleep = True
    elif line[-1] == 'up':
        asleep = False
    else:
        asleep = None

    return [time, guard, asleep]

def time_guard_asleep(guard, records):
    total = timedelta()
    minutes = []
    for i, _ in enumerate(records):
        if records[i][2] == False and records[i][1] == guard:
            total += (records[i][0] - records[i-1][0])
            minutes = [*minutes, *range(records[i-1][0].minute, records[i][0].minute)]
    total = total.total_seconds() / 60
    minute = max(set(minutes), key=minutes.count)
    number_times_slept = minutes.count(minute)
    return total, minute, number_times_slept

def parse_file(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
    lines.sort()
    previous = parse_record(lines[0], [0,0,0])
    records = []
    for i in range(len(lines)):
        records.append(parse_record(lines[i], previous))
        previous = records[i]
    return [record for record in records if record[2] != None]

def find_guard(records, strategy):
    guards = {}
    for guard in [records[x][1] for x, _ in enumerate(records)]:
        guards[guard] = 0

    max = 0
    index = 0
    for guard in guards.keys():
        total, minute, number = time_guard_asleep(guard, records)
        guards[guard] = minute
        if strategy == 1:
            if total > max:
                max = total
                index = guard
        else:
            if number > max:
                max = number
                index = guard
    return index * guards[index]

if __name__ == '__main__':
    args = sys.argv

    filename = args[1]

    records = parse_file(filename)
    print(find_guard(records, 1))
    print(find_guard(records, 2))
