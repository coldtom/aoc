use std::collections::HashSet;

fn load_file<P: AsRef<std::path::Path>>(filename: P) -> Result<Vec<isize>, std::boxed::Box<dyn std::error::Error>> {
    let contents = std::fs::read_to_string(filename)?;
    let mut numbers = Vec::new();

    for s in contents.lines() {
        let number: isize = s.parse()?;
        numbers.push(number);
    }
    Ok(numbers)
}

fn find_first_repeated_frequency(numbers: Vec<isize>) -> isize {
    let mut freq: isize = 0;
    let mut past: HashSet<isize> = HashSet::new();
    let mut pointer: usize = 0;

    while !past.contains(&freq) {
        past.insert(freq);
        freq += numbers[pointer];
        pointer = (pointer + 1) % numbers.len();
    }

    freq
}

fn main() -> Result<(), std::boxed::Box<dyn std::error::Error>> {
    let filename = std::env::args().nth(1).ok_or("Expected Filename")?;
    let contents = load_file(filename)?;

    println!("{}", contents.iter().sum::<isize>());
    println!("{}", find_first_repeated_frequency(contents));

    Ok(())
}